<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | List of IPs that are allowed to access the deploy route
    |--------------------------------------------------------------------------
    |
    | These will be provided by the service you are using to host your remote
    | repository (Github, Bitbucket, etc). just add them to the array. You can
    | specify a range by putting a forward slash b/t two values like 9/15
    |
     */

    'allowed_ips'  => ['131.103.20.160/27', '165.254.145.0/26', '104.192.143.0/24'],

    /*
    |--------------------------------------------------------------------------
    | Branches that should be deployed after a push
    |--------------------------------------------------------------------------
    |
    | This is a list of branches that should be updated when a push is
    | detected.
    |
     */

    'branches'     => ['develop'],

    /*
    |--------------------------------------------------------------------------
    | Project root
    |--------------------------------------------------------------------------
    |
    | The deploy service will navigate to this directory to execute shell
    | commands
    |
     */

    'project_root' => '/var/www/barnes-ats',

    /*
    |--------------------------------------------------------------------------
    | Log File
    |--------------------------------------------------------------------------
    |
    | Custom log file for debugging web hook requests. It will be written to
    | the app/storage/logs directory
    |
     */
    'log_file'     => 'deploy.log',

);

<?php

/**
 * Author: Jackson Dean
 * Date: 7/23/2015
 * Time: 3:38 PM
 */

namespace Barneshc\Simpledeploy {

    use Config;
    use Exception;
    use Monolog\Handler\StreamHandler;
    use Monolog\Logger;
    use Request;
    use App;

    /**
     * Class SimpleDeploy
     * @package Barneshc\Simpledeploy
     */
    class SimpleDeploy
    {
        /**
         * @var
         */
        private $projectDir;
        /**
         * @var array
         */
        private $branches = [ ];
        /**
         * @var
         */
        private $pushedBranch;
        /**
         * @var
         */
        private $repository;
        /**
         * @var Logger
         */
        private $logger;
        /**
         * @var bool
         */
        private $debug;


        /**
         * @param      $payload
         * @param bool $debug
         *
         */
        public function __construct( $payload, $debug = false )
        {
            //set debug output
            $this->debug = $debug;

            //pull in config
            $this->projectDir = Config::get('simpledeploy::project_root');
            $this->branches = Config::get('simpledeploy::branches');
            $logfile = Config::get('simpledeploy::log_file');

            //set up custom log file
            $this->logger = new Logger('Deploy Log');
            $this->logger->pushHandler(new StreamHandler(app_path() . "/storage/logs/{$logfile}", Logger::INFO));

            //throw exception if request IP is invalid
            $this->validateIP(Request::getClientIp());

            //Abort with code 400 if payload is empty
            if ( !isset( $payload ) || empty( $payload ) ) {
                $message = 'Payload was empty. Could not start deploy.';
                $this->logger->addInfo($message);
                App::abort(400, $message);
            }

            if ( $this->debug ) {
                //log payload
                $this->logger->addInfo(
                    '----------PAY LOAD----------' . PHP_EOL .
                    print_r($payload, true) .
                    '----------PAY LOAD----------' . PHP_EOL
                );
            }

            //grab repository data from payload
            $this->repository = $payload->repository->name;
            $this->pushedBranch = $payload->push->changes[0]->new->name;
        }

        /**
         * @param $requestIP
         *
         * @return bool
         */
        private function validateIP( $requestIP )
        {

            $cidrs = Config::get('simpledeploy::allowed_ips');
            $valid = false;

            //if no IPs are whitelisted in config,
            //we will consider any request ip to be valid
            //and short-circuit the validation process
            if ( count($cidrs) === 0 ) {
                return !$valid;
            }

            foreach ( $cidrs as $cidr ) {

                $cidr = explode('/', $cidr);
                $range = [
                    'min' => ( ip2long($cidr[0]) ) & ( ( -1 << ( 32 - (int)$cidr[1] ) ) ),
                    'max' => ( ip2long($cidr[0]) ) + pow(2, ( 32 - (int)$cidr[1] )) - 1,
                ];

                $compareIP = ip2long($requestIP);

                if ( $compareIP >= $range['min'] && $compareIP <= $range['max'] ) {
                    $valid = true;
                    break;
                }
            }

            if ( !$valid ) {
                $message = 'Error: Request originated from an invalid IP: ' . $requestIP;
                $this->logger->addInfo($message);
                App::abort(400, $message);
            }

            return $valid;
        }


        /**
         * @throws Exception
         */
        public function deploy()
        {

            //Abort with code 400 If branch being pushed is not configered for deploy
            if ( !in_array($this->pushedBranch, $this->branches) ) {
                $message = 'Invalid Branch: ' . $this->pushedBranch . ' is not configured for deploy. Operation aborted.';
                $this->logger->addInfo($message);
                App::abort(400, $message);
            }

            if ( $this->debug ) {
                //Log SimpleDeploy instance for debugging
                $this->logger->addInfo(
                    '----------SimpleDeploy Instance----------' . PHP_EOL .
                    $this->debug($this) .
                    '----------SimpleDeploy Instance----------' . PHP_EOL
                );
            }

            $commands = [
                [
                    'cmd' => "cd {$this->projectDir} && pwd 2>&1",
                    'validation' => $this->projectDir,
                ],
                [
                    'cmd' => "sudo git fetch --all 2>&1",
                    'validation' => 'Fetching origin',
                ],
                [
                    'cmd' => "sudo git reset --hard origin/{$this->pushedBranch} 2>&1",
                    'validation' => 'HEAD is now at',
                ],

            ];

            foreach ( $commands as $c ) {
                //log command
                $this->logger->addInfo('Executed: ' . $c['cmd']);

                //Run git shell commands and capture output
                $stdout = shell_exec("{$c['cmd']}");

                $this->logger->addInfo('Stdout: ' . $stdout);

                if ( strpos($stdout, $c['validation']) === false ) {
                    throw new Exception('Error: There was a problem processing shell commands. Please take a look at the deploy log.');
                }
            }
        }


        /**
         * @param $data
         *
         * @return string
         */
        public function debug( $data )
        {
            ob_start();
            var_dump($data);
            return ob_get_clean();
        }

    }
}

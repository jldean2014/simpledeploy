<?php
namespace Barneshc\Simpledeploy;

use Illuminate\Support\ServiceProvider;

class SimpledeployServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events
     * @return void
     */
    public function boot()
    {
        $this->package('barneshc/simpledeploy');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('deploy', function ($app) {
            $request = \Request::instance();
            $payload = \Request::isJson() ? json_decode($request->getContent()) : $request->getContent();
            return new SimpleDeploy($payload);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

}

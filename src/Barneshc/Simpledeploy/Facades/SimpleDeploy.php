<?php

namespace Barneshc\Simpledeploy\Facades;

use Illuminate\Support\Facades\Facade;

class SimpleDeploy extends Facade
{
    /**
     * Get registered name of the package
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'deploy';
    }
}
